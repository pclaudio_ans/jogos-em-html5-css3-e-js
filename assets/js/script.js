(() => {
    const cards = {'pedra': '✊', 'papel': '✋', 'tesoura': '✌'};
    const toggle = document.querySelector('.toggle');
    const userName = document.querySelector('.user-name');
    const modal = document.querySelector('.modal-container');
    const form = document.querySelector('.modal-form');
    const userCardName = document.querySelector('.userCardName');
    const result = document.querySelector('.result');
    const closeResult = document.querySelector('.closeResult');
    const pedraPapelTesoura = document.querySelector('.pedraPapelTesoura');
    const pedraPapelTesouraLink = document.querySelector('.pedraPapelTesouraLink');
    const comingSoon = document.querySelector('.comingSoon');
    const comingSoonLink = document.querySelectorAll('.comingSoonLink');
    const scoreDisplay = document.querySelector('.scoreDisplay');
    
    let score = 0;
    scoreDisplay.innerText = score;

    function clickToggle() {
        toggle.click();
    }

    function setGravatar(email) {
        const hash = CryptoJS.MD5(email).toString();
        const request = new XMLHttpRequest();

        request.open("GET", `https://pt.gravatar.com/${hash}.json`, true);
        request.responseType = 'json';
        request.send();

        request.onload = () => {
            if (request.readyState === 4 && request.status === 200) {
                const gravatarUrl = `${request.response.entry[0].thumbnailUrl}?s=128`;
                const userPhoto = document.querySelector('#userPhoto');
                userPhoto.setAttribute('src',gravatarUrl);
            }
        }
    }

    function getUserChoice(card) {
        for (let i = 0; i < 3; i++) {
            const classes = card.path[i].classList.value.trim();

            if (classes.startsWith('card ')) {
                return classes.substring(classes.indexOf(' ') + 1);
            }
        }
    }

    function getComputerChoice () {
        const choices = ['pedra', 'papel', 'tesoura'];
        const randomChoiceIndex = Math.floor(Math.random() * 3);

        return choices[randomChoiceIndex];
    }

    function startRound(card) {
        const animation = document.querySelector('.animation');
        const pElement = document.createElement('p');

        const resultTitle = document.querySelector('.result-content-title');
        resultTitle.innerText = 'Você perdeu!';

        const userChoice = getUserChoice(card);
        const computerChoice = getComputerChoice();

        const userCard = document.querySelector('.userCard');
        userCard.innerText = cards[userChoice];

        const computerCard = document.querySelector('.computerCard');
        computerCard.innerText = cards[computerChoice];

        let isEven = false;
        let doYouWin = false;

        if (userChoice === computerChoice) {
            isEven = true;
            resultTitle.innerText = 'Empatou!';
        } else if (userChoice === 'pedra' && computerChoice === 'tesoura'
                || userChoice === 'papel' && computerChoice === 'pedra'
                || userChoice === 'tesoura' && computerChoice === 'papel') {
            doYouWin = true;
            score++;
            resultTitle.innerText = 'Você ganhou!';
        }

        if (isEven) {
            console.log(`Empate: ${userChoice} VS ${computerChoice}`);
        } else if (doYouWin) {
            console.log(`Você ganhou: ${userChoice} VS ${computerChoice}`);
        } else {
            console.log(`Você perdeu: ${userChoice} VS ${computerChoice}`);
        }

        animation.classList.remove('hidden');
        
        pElement.innerText = 'Pedra...';
        animation.appendChild(pElement);
        setTimeout(() => {

            pElement.innerText = 'Papel...';
            animation.appendChild(pElement);
            setTimeout(() => {

                pElement.innerText = 'Tesoura!';
                animation.appendChild(pElement);
                setTimeout(() => {

                    animation.removeChild(pElement);
                    animation.classList.add('hidden');
                    result.classList.remove('hidden');
                    scoreDisplay.innerText = score;
                }, 1000);
            }, 1000);
        }, 1000);
    }

    document.querySelectorAll('.navLink').forEach(element => {
        element.addEventListener('click', clickToggle);
    });

    document.querySelectorAll('.card').forEach(element => {
        element.addEventListener('click', startRound);
    }); 

    toggle.addEventListener('click', () => {
        const sideNav = document.querySelector('.sidenav');
        sideNav.classList.toggle('opend');
    });

    form.addEventListener('submit', (event) => {
        event.preventDefault();

        const email = document.querySelector('#email').value.trim();

        if (email !== '') {
            setGravatar(email);
            userName.innerText = email.substring(0, email.indexOf('@'));
            userCardName.innerText = userName.innerHTML;
            modal.style.display = 'none';
        }
    });

    window.addEventListener('click', (event) => {
        if (event.target == result) {
            result.classList.add('hidden');
        }
    });

    closeResult.addEventListener('click', () => {
        result.classList.add('hidden');
    });

    pedraPapelTesouraLink.addEventListener('click', () => {
        if (pedraPapelTesoura.classList.contains('hidden')) {
            comingSoon.classList.add('hidden');
            pedraPapelTesoura.classList.remove('hidden');
        }
    });

    comingSoonLink.forEach(element => {
        element.addEventListener('click', () => {
            if (comingSoon.classList.contains('hidden')) {
                pedraPapelTesoura.classList.add('hidden');
                comingSoon.classList.remove('hidden');
            }
        });
    });
})();